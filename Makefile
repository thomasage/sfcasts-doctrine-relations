EXEC_DOCKER_COMPOSE = docker-compose
EXEC_SYMFONY = symfony

.PHONY = cs help initdb phpstan start stop test

.DEFAULT_GOAL := help

##

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##

initdb: ## Refresh database (drop, create and load fixtures)
	@$(EXEC_SYMFONY) console doctrine:database:drop --if-exists --force \
    && $(EXEC_SYMFONY) console doctrine:database:create \
    && $(EXEC_SYMFONY) console doctrine:schema:update --force \
    && $(EXEC_SYMFONY) console doctrine:fixtures:load -n

start: ## Start dev env
	@$(EXEC_DOCKER_COMPOSE) up --remove-orphans -d \
	&& $(EXEC_SYMFONY) run -d yarn watch \
	&& $(EXEC_SYMFONY) serve -d

stop: ## Stop dev env
	@$(EXEC_SYMFONY) server:stop \
	&& $(EXEC_DOCKER_COMPOSE) stop

##

cs: ## Fix code style
	@$(EXEC_SYMFONY) run vendor/bin/php-cs-fixer fix

phpstan: ## Static analysis
	@$(EXEC_SYMFONY) run vendor/bin/phpstan analyse

test: ## Run tests suite
	@APP_ENV=test $(EXEC_SYMFONY) run bin/phpunit
