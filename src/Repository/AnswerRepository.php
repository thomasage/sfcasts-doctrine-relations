<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Answer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\QueryException;
use Doctrine\Persistence\ManagerRegistry;

final class AnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Answer::class);
    }

    /**
     * @return Answer[]
     *
     * @throws QueryException
     */
    public function findAllApproved(int $max): array
    {
        return $this->createQueryBuilder('answer')
            ->addCriteria(self::createApprovedCriteria())
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    public static function createApprovedCriteria(): Criteria
    {
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq('status', Answer::STATUS_APPROVED));
    }

    /**
     * @return Answer[]
     *
     * @throws QueryException
     */
    public function findMostPopular(?string $search = null): array
    {
        $queryBuilder = $this->createQueryBuilder('answer')
            ->innerJoin('answer.question', 'question')
            ->addSelect('question')
            ->addCriteria(self::createApprovedCriteria())
            ->orderBy('answer.votes', 'DESC');
        if ($search) {
            $queryBuilder
                ->andWhere('answer.content LIKE :searchTerm OR question.question LIKE :searchTerm')
                ->setParameter('searchTerm', "%$search%");
        }

        return $queryBuilder
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
